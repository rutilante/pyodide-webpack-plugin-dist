(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define("PyodidePlugin", [], factory);
	else if(typeof exports === 'object')
		exports["PyodidePlugin"] = factory();
	else
		root["PyodidePlugin"] = factory();
})((typeof self !== 'undefined' ? self : this), () => {
return /******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./index.ts":
/*!******************!*\
  !*** ./index.ts ***!
  \******************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.PyodidePlugin = void 0;
const fs_1 = __importDefault(__webpack_require__(/*! fs */ "fs"));
const assert_1 = __importDefault(__webpack_require__(/*! assert */ "assert"));
const path_1 = __importDefault(__webpack_require__(/*! path */ "path"));
const copy_webpack_plugin_1 = __importDefault(__webpack_require__(/*! copy-webpack-plugin */ "copy-webpack-plugin"));
const patterns = __importStar(__webpack_require__(/*! ./lib/patterns */ "./lib/patterns.ts"));
class PyodidePlugin extends copy_webpack_plugin_1.default {
    constructor(options = {}) {
        let outDirectory = options.outDirectory || "pyodide";
        if (outDirectory.startsWith("/")) {
            outDirectory = outDirectory.slice(1);
        }
        const globalLoadPyodide = options.globalLoadPyodide || false;
        const pyodidePackagePath = tryGetPyodidePath(options.pyodideDependencyPath);
        const pkg = tryResolvePyodidePackage(pyodidePackagePath, options.version);
        options.patterns = patterns.chooseAndTransform(pkg.version, options.packageIndexUrl).map((pattern) => {
            return {
                from: path_1.default.resolve(pyodidePackagePath, pattern.from),
                to: path_1.default.join(outDirectory, pattern.to),
                transform: pattern.transform,
            };
        });
        assert_1.default.ok(options.patterns.length > 0, `Unsupported version of pyodide. Must use >=${patterns.versions[0]}`);
        delete options.packageIndexUrl;
        delete options.globalLoadPyodide;
        super(options);
        this.globalLoadPyodide = globalLoadPyodide;
    }
    apply(compiler) {
        super.apply(compiler);
        if (this.globalLoadPyodide) {
            return;
        }
        // strip global loadPyodide
        compiler.hooks.make.tap(this.constructor.name, (compilation) => {
            compilation.hooks.succeedModule.tap({
                name: "pyodide-webpack-plugin",
                stage: compiler.webpack.Compilation.PROCESS_ASSETS_STAGE_OPTIMIZE_COMPATIBILITY,
            }, (module) => {
                var _a;
                const isPyodide = (_a = module.nameForCondition()) === null || _a === void 0 ? void 0 : _a.match(/pyodide\/pyodide\.m?js$/);
                if (!isPyodide) {
                    return;
                }
                const source = module._source;
                source._value = source.source().replace("globalThis.loadPyodide=loadPyodide", "({})");
            });
        });
    }
}
exports.PyodidePlugin = PyodidePlugin;
/**
 * Try to find the pyodide path. Can't use require.resolve because it is not supported in
 * module builds. Nodes import.meta.resolve is experimental and still very new as of node 19.x
 * This method is works universally under the assumption of an install in node_modules/pyodide
 * @param pyodidePath
 * @returns
 */
function tryGetPyodidePath(pyodidePath) {
    if (pyodidePath) {
        return path_1.default.resolve(pyodidePath);
    }
    const modulePath = path_1.default.resolve("node_modules");
    for (const dependencyPath of fs_1.default.readdirSync(path_1.default.resolve("node_modules"), { withFileTypes: true })) {
        if (dependencyPath.name === "pyodide" && dependencyPath.isDirectory()) {
            return path_1.default.join(modulePath, dependencyPath.name);
        }
    }
    throw new Error(`Unable to resolve pyodide package path in ${modulePath}`);
}
/**
 * Read the pyodide package dependency package.json to return necessary metadata
 * @param version
 * @returns
 */
function tryResolvePyodidePackage(pyodidePath, version) {
    if (version) {
        return { version };
    }
    const pkgPath = path_1.default.resolve(pyodidePath, "package.json");
    try {
        const pkg = fs_1.default.readFileSync(pkgPath, "utf-8");
        return JSON.parse(pkg);
    }
    catch (e) {
        throw new Error(`unable to read package.json from pyodide dependency in ${pkgPath}`);
    }
}
exports["default"] = PyodidePlugin;


/***/ }),

/***/ "./lib/patterns.ts":
/*!*************************!*\
  !*** ./lib/patterns.ts ***!
  \*************************/
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.chooseAndTransform = exports.transform = exports.choose = exports.versions = void 0;
const files = {
    "0.21.3": [
        "distutils.tar",
        "package.json",
        "pyodide_py.tar",
        "pyodide.asm.js",
        "pyodide.asm.data",
        "pyodide.asm.wasm",
        "repodata.json",
    ],
    "0.22.1": [
        "pyodide_py.tar",
        "pyodide.asm.js",
        "pyodide.asm.data",
        "pyodide.asm.wasm",
        "repodata.json",
    ],
};
exports.versions = Object.keys(files);
/**
 * Choose the set of files to match for copying out of pyodide.
 * Based on the version passed. If no version is available in files to match
 * that is great enough an empty array is returned.
 * @param version
 * @returns {string[]}
 */
function choose(version = "0.0.0") {
    let chosen = [];
    for (let i = 0; i < exports.versions.length; i++) {
        if (version >= exports.versions[i]) {
            chosen = files[exports.versions[i]];
        }
    }
    return chosen;
}
exports.choose = choose;
/**
 * Choose the set of files to match for copying out of pyodide.
 * Based on the version passed. If no version is available in files to match
 * that is great enough an empty array is returned.
 * @param version
 * @param pattern
 * @param packageIndexUrl
 * @returns {PyodideObjectPattern[]}
 */
function transform(version, pattern, packageIndexUrl) {
    return pattern.map((name) => {
        let transform;
        if (packageIndexUrl && name == "pyodide.asm.js") {
            transform = {
                transformer: (input) => {
                    return input
                        .toString()
                        .replace("resolvePath(file_name,API.config.indexURL)", `resolvePath(file_name,"${packageIndexUrl}")`);
                },
            };
        }
        return { from: name, to: name, transform };
    });
}
exports.transform = transform;
function chooseAndTransform(version = "0.0.0", packageIndexUrl) {
    packageIndexUrl = packageIndexUrl !== null && packageIndexUrl !== void 0 ? packageIndexUrl : `https://cdn.jsdelivr.net/pyodide/v${version}/full/`;
    return transform(version, choose(version), packageIndexUrl);
}
exports.chooseAndTransform = chooseAndTransform;


/***/ }),

/***/ "copy-webpack-plugin":
/*!**************************************!*\
  !*** external "copy-webpack-plugin" ***!
  \**************************************/
/***/ ((module) => {

module.exports = require("copy-webpack-plugin");

/***/ }),

/***/ "assert":
/*!*************************!*\
  !*** external "assert" ***!
  \*************************/
/***/ ((module) => {

module.exports = require("assert");

/***/ }),

/***/ "fs":
/*!*********************!*\
  !*** external "fs" ***!
  \*********************/
/***/ ((module) => {

module.exports = require("fs");

/***/ }),

/***/ "path":
/*!***********************!*\
  !*** external "path" ***!
  \***********************/
/***/ ((module) => {

module.exports = require("path");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module is referenced by other modules so it can't be inlined
/******/ 	var __webpack_exports__ = __webpack_require__("./index.ts");
/******/ 	
/******/ 	return __webpack_exports__;
/******/ })()
;
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGx1Z2luLmpzIiwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRCxPOzs7Ozs7Ozs7O0FDVmE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsb0NBQW9DO0FBQ25EO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBLDBDQUEwQyw0QkFBNEI7QUFDdEUsQ0FBQztBQUNEO0FBQ0EsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2Q0FBNkM7QUFDN0M7QUFDQSw4Q0FBNkMsRUFBRSxhQUFhLEVBQUM7QUFDN0QscUJBQXFCO0FBQ3JCLDZCQUE2QixtQkFBTyxDQUFDLGNBQUk7QUFDekMsaUNBQWlDLG1CQUFPLENBQUMsc0JBQVE7QUFDakQsK0JBQStCLG1CQUFPLENBQUMsa0JBQU07QUFDN0MsOENBQThDLG1CQUFPLENBQUMsZ0RBQXFCO0FBQzNFLDhCQUE4QixtQkFBTyxDQUFDLHlDQUFnQjtBQUN0RDtBQUNBLDRCQUE0QjtBQUM1QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVCx1R0FBdUcscUJBQXFCO0FBQzVIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrR0FBa0c7QUFDbEcsYUFBYTtBQUNiLFNBQVM7QUFDVDtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9HQUFvRyxxQkFBcUI7QUFDekg7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpRUFBaUUsV0FBVztBQUM1RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0ZBQWtGLFFBQVE7QUFDMUY7QUFDQTtBQUNBLGtCQUFlOzs7Ozs7Ozs7OztBQ3BIRjtBQUNiLDhDQUE2QyxFQUFFLGFBQWEsRUFBQztBQUM3RCwwQkFBMEIsR0FBRyxpQkFBaUIsR0FBRyxjQUFjLEdBQUcsZ0JBQWdCO0FBQ2xGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdCQUFnQjtBQUNoQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBLG9CQUFvQiw2QkFBNkI7QUFDakQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsY0FBYztBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlHQUF5RyxnQkFBZ0I7QUFDekgsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakIsS0FBSztBQUNMO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0Esc0lBQXNJLFFBQVE7QUFDOUk7QUFDQTtBQUNBLDBCQUEwQjs7Ozs7Ozs7Ozs7QUNwRTFCOzs7Ozs7Ozs7O0FDQUE7Ozs7Ozs7Ozs7QUNBQTs7Ozs7Ozs7OztBQ0FBOzs7Ozs7VUNBQTtVQUNBOztVQUVBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBOztVQUVBO1VBQ0E7O1VBRUE7VUFDQTtVQUNBOzs7O1VFdEJBO1VBQ0E7VUFDQTtVQUNBIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vUHlvZGlkZVBsdWdpbi93ZWJwYWNrL3VuaXZlcnNhbE1vZHVsZURlZmluaXRpb24iLCJ3ZWJwYWNrOi8vUHlvZGlkZVBsdWdpbi8uL2luZGV4LnRzIiwid2VicGFjazovL1B5b2RpZGVQbHVnaW4vLi9saWIvcGF0dGVybnMudHMiLCJ3ZWJwYWNrOi8vUHlvZGlkZVBsdWdpbi9leHRlcm5hbCBjb21tb25qcyBcImNvcHktd2VicGFjay1wbHVnaW5cIiIsIndlYnBhY2s6Ly9QeW9kaWRlUGx1Z2luL2V4dGVybmFsIG5vZGUtY29tbW9uanMgXCJhc3NlcnRcIiIsIndlYnBhY2s6Ly9QeW9kaWRlUGx1Z2luL2V4dGVybmFsIG5vZGUtY29tbW9uanMgXCJmc1wiIiwid2VicGFjazovL1B5b2RpZGVQbHVnaW4vZXh0ZXJuYWwgbm9kZS1jb21tb25qcyBcInBhdGhcIiIsIndlYnBhY2s6Ly9QeW9kaWRlUGx1Z2luL3dlYnBhY2svYm9vdHN0cmFwIiwid2VicGFjazovL1B5b2RpZGVQbHVnaW4vd2VicGFjay9iZWZvcmUtc3RhcnR1cCIsIndlYnBhY2s6Ly9QeW9kaWRlUGx1Z2luL3dlYnBhY2svc3RhcnR1cCIsIndlYnBhY2s6Ly9QeW9kaWRlUGx1Z2luL3dlYnBhY2svYWZ0ZXItc3RhcnR1cCJdLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gd2VicGFja1VuaXZlcnNhbE1vZHVsZURlZmluaXRpb24ocm9vdCwgZmFjdG9yeSkge1xuXHRpZih0eXBlb2YgZXhwb3J0cyA9PT0gJ29iamVjdCcgJiYgdHlwZW9mIG1vZHVsZSA9PT0gJ29iamVjdCcpXG5cdFx0bW9kdWxlLmV4cG9ydHMgPSBmYWN0b3J5KCk7XG5cdGVsc2UgaWYodHlwZW9mIGRlZmluZSA9PT0gJ2Z1bmN0aW9uJyAmJiBkZWZpbmUuYW1kKVxuXHRcdGRlZmluZShcIlB5b2RpZGVQbHVnaW5cIiwgW10sIGZhY3RvcnkpO1xuXHRlbHNlIGlmKHR5cGVvZiBleHBvcnRzID09PSAnb2JqZWN0Jylcblx0XHRleHBvcnRzW1wiUHlvZGlkZVBsdWdpblwiXSA9IGZhY3RvcnkoKTtcblx0ZWxzZVxuXHRcdHJvb3RbXCJQeW9kaWRlUGx1Z2luXCJdID0gZmFjdG9yeSgpO1xufSkoKHR5cGVvZiBzZWxmICE9PSAndW5kZWZpbmVkJyA/IHNlbGYgOiB0aGlzKSwgKCkgPT4ge1xucmV0dXJuICIsIlwidXNlIHN0cmljdFwiO1xudmFyIF9fY3JlYXRlQmluZGluZyA9ICh0aGlzICYmIHRoaXMuX19jcmVhdGVCaW5kaW5nKSB8fCAoT2JqZWN0LmNyZWF0ZSA/IChmdW5jdGlvbihvLCBtLCBrLCBrMikge1xuICAgIGlmIChrMiA9PT0gdW5kZWZpbmVkKSBrMiA9IGs7XG4gICAgdmFyIGRlc2MgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKG0sIGspO1xuICAgIGlmICghZGVzYyB8fCAoXCJnZXRcIiBpbiBkZXNjID8gIW0uX19lc01vZHVsZSA6IGRlc2Mud3JpdGFibGUgfHwgZGVzYy5jb25maWd1cmFibGUpKSB7XG4gICAgICBkZXNjID0geyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGZ1bmN0aW9uKCkgeyByZXR1cm4gbVtrXTsgfSB9O1xuICAgIH1cbiAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkobywgazIsIGRlc2MpO1xufSkgOiAoZnVuY3Rpb24obywgbSwgaywgazIpIHtcbiAgICBpZiAoazIgPT09IHVuZGVmaW5lZCkgazIgPSBrO1xuICAgIG9bazJdID0gbVtrXTtcbn0pKTtcbnZhciBfX3NldE1vZHVsZURlZmF1bHQgPSAodGhpcyAmJiB0aGlzLl9fc2V0TW9kdWxlRGVmYXVsdCkgfHwgKE9iamVjdC5jcmVhdGUgPyAoZnVuY3Rpb24obywgdikge1xuICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShvLCBcImRlZmF1bHRcIiwgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdiB9KTtcbn0pIDogZnVuY3Rpb24obywgdikge1xuICAgIG9bXCJkZWZhdWx0XCJdID0gdjtcbn0pO1xudmFyIF9faW1wb3J0U3RhciA9ICh0aGlzICYmIHRoaXMuX19pbXBvcnRTdGFyKSB8fCBmdW5jdGlvbiAobW9kKSB7XG4gICAgaWYgKG1vZCAmJiBtb2QuX19lc01vZHVsZSkgcmV0dXJuIG1vZDtcbiAgICB2YXIgcmVzdWx0ID0ge307XG4gICAgaWYgKG1vZCAhPSBudWxsKSBmb3IgKHZhciBrIGluIG1vZCkgaWYgKGsgIT09IFwiZGVmYXVsdFwiICYmIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChtb2QsIGspKSBfX2NyZWF0ZUJpbmRpbmcocmVzdWx0LCBtb2QsIGspO1xuICAgIF9fc2V0TW9kdWxlRGVmYXVsdChyZXN1bHQsIG1vZCk7XG4gICAgcmV0dXJuIHJlc3VsdDtcbn07XG52YXIgX19pbXBvcnREZWZhdWx0ID0gKHRoaXMgJiYgdGhpcy5fX2ltcG9ydERlZmF1bHQpIHx8IGZ1bmN0aW9uIChtb2QpIHtcbiAgICByZXR1cm4gKG1vZCAmJiBtb2QuX19lc01vZHVsZSkgPyBtb2QgOiB7IFwiZGVmYXVsdFwiOiBtb2QgfTtcbn07XG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHsgdmFsdWU6IHRydWUgfSk7XG5leHBvcnRzLlB5b2RpZGVQbHVnaW4gPSB2b2lkIDA7XG5jb25zdCBmc18xID0gX19pbXBvcnREZWZhdWx0KHJlcXVpcmUoXCJmc1wiKSk7XG5jb25zdCBhc3NlcnRfMSA9IF9faW1wb3J0RGVmYXVsdChyZXF1aXJlKFwiYXNzZXJ0XCIpKTtcbmNvbnN0IHBhdGhfMSA9IF9faW1wb3J0RGVmYXVsdChyZXF1aXJlKFwicGF0aFwiKSk7XG5jb25zdCBjb3B5X3dlYnBhY2tfcGx1Z2luXzEgPSBfX2ltcG9ydERlZmF1bHQocmVxdWlyZShcImNvcHktd2VicGFjay1wbHVnaW5cIikpO1xuY29uc3QgcGF0dGVybnMgPSBfX2ltcG9ydFN0YXIocmVxdWlyZShcIi4vbGliL3BhdHRlcm5zXCIpKTtcbmNsYXNzIFB5b2RpZGVQbHVnaW4gZXh0ZW5kcyBjb3B5X3dlYnBhY2tfcGx1Z2luXzEuZGVmYXVsdCB7XG4gICAgY29uc3RydWN0b3Iob3B0aW9ucyA9IHt9KSB7XG4gICAgICAgIGxldCBvdXREaXJlY3RvcnkgPSBvcHRpb25zLm91dERpcmVjdG9yeSB8fCBcInB5b2RpZGVcIjtcbiAgICAgICAgaWYgKG91dERpcmVjdG9yeS5zdGFydHNXaXRoKFwiL1wiKSkge1xuICAgICAgICAgICAgb3V0RGlyZWN0b3J5ID0gb3V0RGlyZWN0b3J5LnNsaWNlKDEpO1xuICAgICAgICB9XG4gICAgICAgIGNvbnN0IGdsb2JhbExvYWRQeW9kaWRlID0gb3B0aW9ucy5nbG9iYWxMb2FkUHlvZGlkZSB8fCBmYWxzZTtcbiAgICAgICAgY29uc3QgcHlvZGlkZVBhY2thZ2VQYXRoID0gdHJ5R2V0UHlvZGlkZVBhdGgob3B0aW9ucy5weW9kaWRlRGVwZW5kZW5jeVBhdGgpO1xuICAgICAgICBjb25zdCBwa2cgPSB0cnlSZXNvbHZlUHlvZGlkZVBhY2thZ2UocHlvZGlkZVBhY2thZ2VQYXRoLCBvcHRpb25zLnZlcnNpb24pO1xuICAgICAgICBvcHRpb25zLnBhdHRlcm5zID0gcGF0dGVybnMuY2hvb3NlQW5kVHJhbnNmb3JtKHBrZy52ZXJzaW9uLCBvcHRpb25zLnBhY2thZ2VJbmRleFVybCkubWFwKChwYXR0ZXJuKSA9PiB7XG4gICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgIGZyb206IHBhdGhfMS5kZWZhdWx0LnJlc29sdmUocHlvZGlkZVBhY2thZ2VQYXRoLCBwYXR0ZXJuLmZyb20pLFxuICAgICAgICAgICAgICAgIHRvOiBwYXRoXzEuZGVmYXVsdC5qb2luKG91dERpcmVjdG9yeSwgcGF0dGVybi50byksXG4gICAgICAgICAgICAgICAgdHJhbnNmb3JtOiBwYXR0ZXJuLnRyYW5zZm9ybSxcbiAgICAgICAgICAgIH07XG4gICAgICAgIH0pO1xuICAgICAgICBhc3NlcnRfMS5kZWZhdWx0Lm9rKG9wdGlvbnMucGF0dGVybnMubGVuZ3RoID4gMCwgYFVuc3VwcG9ydGVkIHZlcnNpb24gb2YgcHlvZGlkZS4gTXVzdCB1c2UgPj0ke3BhdHRlcm5zLnZlcnNpb25zWzBdfWApO1xuICAgICAgICBkZWxldGUgb3B0aW9ucy5wYWNrYWdlSW5kZXhVcmw7XG4gICAgICAgIGRlbGV0ZSBvcHRpb25zLmdsb2JhbExvYWRQeW9kaWRlO1xuICAgICAgICBzdXBlcihvcHRpb25zKTtcbiAgICAgICAgdGhpcy5nbG9iYWxMb2FkUHlvZGlkZSA9IGdsb2JhbExvYWRQeW9kaWRlO1xuICAgIH1cbiAgICBhcHBseShjb21waWxlcikge1xuICAgICAgICBzdXBlci5hcHBseShjb21waWxlcik7XG4gICAgICAgIGlmICh0aGlzLmdsb2JhbExvYWRQeW9kaWRlKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgLy8gc3RyaXAgZ2xvYmFsIGxvYWRQeW9kaWRlXG4gICAgICAgIGNvbXBpbGVyLmhvb2tzLm1ha2UudGFwKHRoaXMuY29uc3RydWN0b3IubmFtZSwgKGNvbXBpbGF0aW9uKSA9PiB7XG4gICAgICAgICAgICBjb21waWxhdGlvbi5ob29rcy5zdWNjZWVkTW9kdWxlLnRhcCh7XG4gICAgICAgICAgICAgICAgbmFtZTogXCJweW9kaWRlLXdlYnBhY2stcGx1Z2luXCIsXG4gICAgICAgICAgICAgICAgc3RhZ2U6IGNvbXBpbGVyLndlYnBhY2suQ29tcGlsYXRpb24uUFJPQ0VTU19BU1NFVFNfU1RBR0VfT1BUSU1JWkVfQ09NUEFUSUJJTElUWSxcbiAgICAgICAgICAgIH0sIChtb2R1bGUpID0+IHtcbiAgICAgICAgICAgICAgICB2YXIgX2E7XG4gICAgICAgICAgICAgICAgY29uc3QgaXNQeW9kaWRlID0gKF9hID0gbW9kdWxlLm5hbWVGb3JDb25kaXRpb24oKSkgPT09IG51bGwgfHwgX2EgPT09IHZvaWQgMCA/IHZvaWQgMCA6IF9hLm1hdGNoKC9weW9kaWRlXFwvcHlvZGlkZVxcLm0/anMkLyk7XG4gICAgICAgICAgICAgICAgaWYgKCFpc1B5b2RpZGUpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBjb25zdCBzb3VyY2UgPSBtb2R1bGUuX3NvdXJjZTtcbiAgICAgICAgICAgICAgICBzb3VyY2UuX3ZhbHVlID0gc291cmNlLnNvdXJjZSgpLnJlcGxhY2UoXCJnbG9iYWxUaGlzLmxvYWRQeW9kaWRlPWxvYWRQeW9kaWRlXCIsIFwiKHt9KVwiKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG59XG5leHBvcnRzLlB5b2RpZGVQbHVnaW4gPSBQeW9kaWRlUGx1Z2luO1xuLyoqXG4gKiBUcnkgdG8gZmluZCB0aGUgcHlvZGlkZSBwYXRoLiBDYW4ndCB1c2UgcmVxdWlyZS5yZXNvbHZlIGJlY2F1c2UgaXQgaXMgbm90IHN1cHBvcnRlZCBpblxuICogbW9kdWxlIGJ1aWxkcy4gTm9kZXMgaW1wb3J0Lm1ldGEucmVzb2x2ZSBpcyBleHBlcmltZW50YWwgYW5kIHN0aWxsIHZlcnkgbmV3IGFzIG9mIG5vZGUgMTkueFxuICogVGhpcyBtZXRob2QgaXMgd29ya3MgdW5pdmVyc2FsbHkgdW5kZXIgdGhlIGFzc3VtcHRpb24gb2YgYW4gaW5zdGFsbCBpbiBub2RlX21vZHVsZXMvcHlvZGlkZVxuICogQHBhcmFtIHB5b2RpZGVQYXRoXG4gKiBAcmV0dXJuc1xuICovXG5mdW5jdGlvbiB0cnlHZXRQeW9kaWRlUGF0aChweW9kaWRlUGF0aCkge1xuICAgIGlmIChweW9kaWRlUGF0aCkge1xuICAgICAgICByZXR1cm4gcGF0aF8xLmRlZmF1bHQucmVzb2x2ZShweW9kaWRlUGF0aCk7XG4gICAgfVxuICAgIGNvbnN0IG1vZHVsZVBhdGggPSBwYXRoXzEuZGVmYXVsdC5yZXNvbHZlKFwibm9kZV9tb2R1bGVzXCIpO1xuICAgIGZvciAoY29uc3QgZGVwZW5kZW5jeVBhdGggb2YgZnNfMS5kZWZhdWx0LnJlYWRkaXJTeW5jKHBhdGhfMS5kZWZhdWx0LnJlc29sdmUoXCJub2RlX21vZHVsZXNcIiksIHsgd2l0aEZpbGVUeXBlczogdHJ1ZSB9KSkge1xuICAgICAgICBpZiAoZGVwZW5kZW5jeVBhdGgubmFtZSA9PT0gXCJweW9kaWRlXCIgJiYgZGVwZW5kZW5jeVBhdGguaXNEaXJlY3RvcnkoKSkge1xuICAgICAgICAgICAgcmV0dXJuIHBhdGhfMS5kZWZhdWx0LmpvaW4obW9kdWxlUGF0aCwgZGVwZW5kZW5jeVBhdGgubmFtZSk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgdGhyb3cgbmV3IEVycm9yKGBVbmFibGUgdG8gcmVzb2x2ZSBweW9kaWRlIHBhY2thZ2UgcGF0aCBpbiAke21vZHVsZVBhdGh9YCk7XG59XG4vKipcbiAqIFJlYWQgdGhlIHB5b2RpZGUgcGFja2FnZSBkZXBlbmRlbmN5IHBhY2thZ2UuanNvbiB0byByZXR1cm4gbmVjZXNzYXJ5IG1ldGFkYXRhXG4gKiBAcGFyYW0gdmVyc2lvblxuICogQHJldHVybnNcbiAqL1xuZnVuY3Rpb24gdHJ5UmVzb2x2ZVB5b2RpZGVQYWNrYWdlKHB5b2RpZGVQYXRoLCB2ZXJzaW9uKSB7XG4gICAgaWYgKHZlcnNpb24pIHtcbiAgICAgICAgcmV0dXJuIHsgdmVyc2lvbiB9O1xuICAgIH1cbiAgICBjb25zdCBwa2dQYXRoID0gcGF0aF8xLmRlZmF1bHQucmVzb2x2ZShweW9kaWRlUGF0aCwgXCJwYWNrYWdlLmpzb25cIik7XG4gICAgdHJ5IHtcbiAgICAgICAgY29uc3QgcGtnID0gZnNfMS5kZWZhdWx0LnJlYWRGaWxlU3luYyhwa2dQYXRoLCBcInV0Zi04XCIpO1xuICAgICAgICByZXR1cm4gSlNPTi5wYXJzZShwa2cpO1xuICAgIH1cbiAgICBjYXRjaCAoZSkge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoYHVuYWJsZSB0byByZWFkIHBhY2thZ2UuanNvbiBmcm9tIHB5b2RpZGUgZGVwZW5kZW5jeSBpbiAke3BrZ1BhdGh9YCk7XG4gICAgfVxufVxuZXhwb3J0cy5kZWZhdWx0ID0gUHlvZGlkZVBsdWdpbjtcbiIsIlwidXNlIHN0cmljdFwiO1xuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7IHZhbHVlOiB0cnVlIH0pO1xuZXhwb3J0cy5jaG9vc2VBbmRUcmFuc2Zvcm0gPSBleHBvcnRzLnRyYW5zZm9ybSA9IGV4cG9ydHMuY2hvb3NlID0gZXhwb3J0cy52ZXJzaW9ucyA9IHZvaWQgMDtcbmNvbnN0IGZpbGVzID0ge1xuICAgIFwiMC4yMS4zXCI6IFtcbiAgICAgICAgXCJkaXN0dXRpbHMudGFyXCIsXG4gICAgICAgIFwicGFja2FnZS5qc29uXCIsXG4gICAgICAgIFwicHlvZGlkZV9weS50YXJcIixcbiAgICAgICAgXCJweW9kaWRlLmFzbS5qc1wiLFxuICAgICAgICBcInB5b2RpZGUuYXNtLmRhdGFcIixcbiAgICAgICAgXCJweW9kaWRlLmFzbS53YXNtXCIsXG4gICAgICAgIFwicmVwb2RhdGEuanNvblwiLFxuICAgIF0sXG4gICAgXCIwLjIyLjFcIjogW1xuICAgICAgICBcInB5b2RpZGVfcHkudGFyXCIsXG4gICAgICAgIFwicHlvZGlkZS5hc20uanNcIixcbiAgICAgICAgXCJweW9kaWRlLmFzbS5kYXRhXCIsXG4gICAgICAgIFwicHlvZGlkZS5hc20ud2FzbVwiLFxuICAgICAgICBcInJlcG9kYXRhLmpzb25cIixcbiAgICBdLFxufTtcbmV4cG9ydHMudmVyc2lvbnMgPSBPYmplY3Qua2V5cyhmaWxlcyk7XG4vKipcbiAqIENob29zZSB0aGUgc2V0IG9mIGZpbGVzIHRvIG1hdGNoIGZvciBjb3B5aW5nIG91dCBvZiBweW9kaWRlLlxuICogQmFzZWQgb24gdGhlIHZlcnNpb24gcGFzc2VkLiBJZiBubyB2ZXJzaW9uIGlzIGF2YWlsYWJsZSBpbiBmaWxlcyB0byBtYXRjaFxuICogdGhhdCBpcyBncmVhdCBlbm91Z2ggYW4gZW1wdHkgYXJyYXkgaXMgcmV0dXJuZWQuXG4gKiBAcGFyYW0gdmVyc2lvblxuICogQHJldHVybnMge3N0cmluZ1tdfVxuICovXG5mdW5jdGlvbiBjaG9vc2UodmVyc2lvbiA9IFwiMC4wLjBcIikge1xuICAgIGxldCBjaG9zZW4gPSBbXTtcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IGV4cG9ydHMudmVyc2lvbnMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgaWYgKHZlcnNpb24gPj0gZXhwb3J0cy52ZXJzaW9uc1tpXSkge1xuICAgICAgICAgICAgY2hvc2VuID0gZmlsZXNbZXhwb3J0cy52ZXJzaW9uc1tpXV07XG4gICAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIGNob3Nlbjtcbn1cbmV4cG9ydHMuY2hvb3NlID0gY2hvb3NlO1xuLyoqXG4gKiBDaG9vc2UgdGhlIHNldCBvZiBmaWxlcyB0byBtYXRjaCBmb3IgY29weWluZyBvdXQgb2YgcHlvZGlkZS5cbiAqIEJhc2VkIG9uIHRoZSB2ZXJzaW9uIHBhc3NlZC4gSWYgbm8gdmVyc2lvbiBpcyBhdmFpbGFibGUgaW4gZmlsZXMgdG8gbWF0Y2hcbiAqIHRoYXQgaXMgZ3JlYXQgZW5vdWdoIGFuIGVtcHR5IGFycmF5IGlzIHJldHVybmVkLlxuICogQHBhcmFtIHZlcnNpb25cbiAqIEBwYXJhbSBwYXR0ZXJuXG4gKiBAcGFyYW0gcGFja2FnZUluZGV4VXJsXG4gKiBAcmV0dXJucyB7UHlvZGlkZU9iamVjdFBhdHRlcm5bXX1cbiAqL1xuZnVuY3Rpb24gdHJhbnNmb3JtKHZlcnNpb24sIHBhdHRlcm4sIHBhY2thZ2VJbmRleFVybCkge1xuICAgIHJldHVybiBwYXR0ZXJuLm1hcCgobmFtZSkgPT4ge1xuICAgICAgICBsZXQgdHJhbnNmb3JtO1xuICAgICAgICBpZiAocGFja2FnZUluZGV4VXJsICYmIG5hbWUgPT0gXCJweW9kaWRlLmFzbS5qc1wiKSB7XG4gICAgICAgICAgICB0cmFuc2Zvcm0gPSB7XG4gICAgICAgICAgICAgICAgdHJhbnNmb3JtZXI6IChpbnB1dCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gaW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgIC50b1N0cmluZygpXG4gICAgICAgICAgICAgICAgICAgICAgICAucmVwbGFjZShcInJlc29sdmVQYXRoKGZpbGVfbmFtZSxBUEkuY29uZmlnLmluZGV4VVJMKVwiLCBgcmVzb2x2ZVBhdGgoZmlsZV9uYW1lLFwiJHtwYWNrYWdlSW5kZXhVcmx9XCIpYCk7XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIH07XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHsgZnJvbTogbmFtZSwgdG86IG5hbWUsIHRyYW5zZm9ybSB9O1xuICAgIH0pO1xufVxuZXhwb3J0cy50cmFuc2Zvcm0gPSB0cmFuc2Zvcm07XG5mdW5jdGlvbiBjaG9vc2VBbmRUcmFuc2Zvcm0odmVyc2lvbiA9IFwiMC4wLjBcIiwgcGFja2FnZUluZGV4VXJsKSB7XG4gICAgcGFja2FnZUluZGV4VXJsID0gcGFja2FnZUluZGV4VXJsICE9PSBudWxsICYmIHBhY2thZ2VJbmRleFVybCAhPT0gdm9pZCAwID8gcGFja2FnZUluZGV4VXJsIDogYGh0dHBzOi8vY2RuLmpzZGVsaXZyLm5ldC9weW9kaWRlL3Yke3ZlcnNpb259L2Z1bGwvYDtcbiAgICByZXR1cm4gdHJhbnNmb3JtKHZlcnNpb24sIGNob29zZSh2ZXJzaW9uKSwgcGFja2FnZUluZGV4VXJsKTtcbn1cbmV4cG9ydHMuY2hvb3NlQW5kVHJhbnNmb3JtID0gY2hvb3NlQW5kVHJhbnNmb3JtO1xuIiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiY29weS13ZWJwYWNrLXBsdWdpblwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJhc3NlcnRcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiZnNcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicGF0aFwiKTsiLCIvLyBUaGUgbW9kdWxlIGNhY2hlXG52YXIgX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fID0ge307XG5cbi8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG5mdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuXHR2YXIgY2FjaGVkTW9kdWxlID0gX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fW21vZHVsZUlkXTtcblx0aWYgKGNhY2hlZE1vZHVsZSAhPT0gdW5kZWZpbmVkKSB7XG5cdFx0cmV0dXJuIGNhY2hlZE1vZHVsZS5leHBvcnRzO1xuXHR9XG5cdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG5cdHZhciBtb2R1bGUgPSBfX3dlYnBhY2tfbW9kdWxlX2NhY2hlX19bbW9kdWxlSWRdID0ge1xuXHRcdC8vIG5vIG1vZHVsZS5pZCBuZWVkZWRcblx0XHQvLyBubyBtb2R1bGUubG9hZGVkIG5lZWRlZFxuXHRcdGV4cG9ydHM6IHt9XG5cdH07XG5cblx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG5cdF9fd2VicGFja19tb2R1bGVzX19bbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG5cdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG5cdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbn1cblxuIiwiIiwiLy8gc3RhcnR1cFxuLy8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4vLyBUaGlzIGVudHJ5IG1vZHVsZSBpcyByZWZlcmVuY2VkIGJ5IG90aGVyIG1vZHVsZXMgc28gaXQgY2FuJ3QgYmUgaW5saW5lZFxudmFyIF9fd2VicGFja19leHBvcnRzX18gPSBfX3dlYnBhY2tfcmVxdWlyZV9fKFwiLi9pbmRleC50c1wiKTtcbiIsIiJdLCJuYW1lcyI6W10sInNvdXJjZVJvb3QiOiIifQ==