import * as __WEBPACK_EXTERNAL_MODULE_copy_webpack_plugin_c1a9f86d__ from "copy-webpack-plugin";
import { createRequire as __WEBPACK_EXTERNAL_createRequire } from "module";
/******/ var __webpack_modules__ = ({

/***/ "./lib/patterns.ts":
/*!*************************!*\
  !*** ./lib/patterns.ts ***!
  \*************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "choose": () => (/* binding */ choose),
/* harmony export */   "chooseAndTransform": () => (/* binding */ chooseAndTransform),
/* harmony export */   "transform": () => (/* binding */ transform),
/* harmony export */   "versions": () => (/* binding */ versions)
/* harmony export */ });
const files = {
    "0.21.3": [
        "distutils.tar",
        "package.json",
        "pyodide_py.tar",
        "pyodide.asm.js",
        "pyodide.asm.data",
        "pyodide.asm.wasm",
        "repodata.json",
    ],
    "0.22.1": [
        "pyodide_py.tar",
        "pyodide.asm.js",
        "pyodide.asm.data",
        "pyodide.asm.wasm",
        "repodata.json",
    ],
};
const versions = Object.keys(files);
/**
 * Choose the set of files to match for copying out of pyodide.
 * Based on the version passed. If no version is available in files to match
 * that is great enough an empty array is returned.
 * @param version
 * @returns {string[]}
 */
function choose(version = "0.0.0") {
    let chosen = [];
    for (let i = 0; i < versions.length; i++) {
        if (version >= versions[i]) {
            chosen = files[versions[i]];
        }
    }
    return chosen;
}
/**
 * Choose the set of files to match for copying out of pyodide.
 * Based on the version passed. If no version is available in files to match
 * that is great enough an empty array is returned.
 * @param version
 * @param pattern
 * @param packageIndexUrl
 * @returns {PyodideObjectPattern[]}
 */
function transform(version, pattern, packageIndexUrl) {
    return pattern.map((name) => {
        let transform;
        if (packageIndexUrl && name == "pyodide.asm.js") {
            transform = {
                transformer: (input) => {
                    return input
                        .toString()
                        .replace("resolvePath(file_name,API.config.indexURL)", `resolvePath(file_name,"${packageIndexUrl}")`);
                },
            };
        }
        return { from: name, to: name, transform };
    });
}
function chooseAndTransform(version = "0.0.0", packageIndexUrl) {
    packageIndexUrl = packageIndexUrl ?? `https://cdn.jsdelivr.net/pyodide/v${version}/full/`;
    return transform(version, choose(version), packageIndexUrl);
}


/***/ }),

/***/ "copy-webpack-plugin":
/*!**************************************!*\
  !*** external "copy-webpack-plugin" ***!
  \**************************************/
/***/ ((module) => {

var x = y => { var x = {}; __webpack_require__.d(x, y); return x; }
var y = x => () => x
module.exports = __WEBPACK_EXTERNAL_MODULE_copy_webpack_plugin_c1a9f86d__;

/***/ }),

/***/ "assert":
/*!*************************!*\
  !*** external "assert" ***!
  \*************************/
/***/ ((module) => {

module.exports = __WEBPACK_EXTERNAL_createRequire(import.meta.url)("assert");

/***/ }),

/***/ "fs":
/*!*********************!*\
  !*** external "fs" ***!
  \*********************/
/***/ ((module) => {

module.exports = __WEBPACK_EXTERNAL_createRequire(import.meta.url)("fs");

/***/ }),

/***/ "path":
/*!***********************!*\
  !*** external "path" ***!
  \***********************/
/***/ ((module) => {

module.exports = __WEBPACK_EXTERNAL_createRequire(import.meta.url)("path");

/***/ })

/******/ });
/************************************************************************/
/******/ // The module cache
/******/ var __webpack_module_cache__ = {};
/******/ 
/******/ // The require function
/******/ function __webpack_require__(moduleId) {
/******/ 	// Check if module is in cache
/******/ 	var cachedModule = __webpack_module_cache__[moduleId];
/******/ 	if (cachedModule !== undefined) {
/******/ 		return cachedModule.exports;
/******/ 	}
/******/ 	// Create a new module (and put it into the cache)
/******/ 	var module = __webpack_module_cache__[moduleId] = {
/******/ 		// no module.id needed
/******/ 		// no module.loaded needed
/******/ 		exports: {}
/******/ 	};
/******/ 
/******/ 	// Execute the module function
/******/ 	__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 
/******/ 	// Return the exports of the module
/******/ 	return module.exports;
/******/ }
/******/ 
/************************************************************************/
/******/ /* webpack/runtime/compat get default export */
/******/ (() => {
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = (module) => {
/******/ 		var getter = module && module.__esModule ?
/******/ 			() => (module['default']) :
/******/ 			() => (module);
/******/ 		__webpack_require__.d(getter, { a: getter });
/******/ 		return getter;
/******/ 	};
/******/ })();
/******/ 
/******/ /* webpack/runtime/define property getters */
/******/ (() => {
/******/ 	// define getter functions for harmony exports
/******/ 	__webpack_require__.d = (exports, definition) => {
/******/ 		for(var key in definition) {
/******/ 			if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 				Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 			}
/******/ 		}
/******/ 	};
/******/ })();
/******/ 
/******/ /* webpack/runtime/hasOwnProperty shorthand */
/******/ (() => {
/******/ 	__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ })();
/******/ 
/******/ /* webpack/runtime/make namespace object */
/******/ (() => {
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = (exports) => {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/ })();
/******/ 
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
/*!******************!*\
  !*** ./index.ts ***!
  \******************/
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PyodidePlugin": () => (/* binding */ PyodidePlugin),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! fs */ "fs");
/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(fs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var assert__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! assert */ "assert");
/* harmony import */ var assert__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(assert__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! path */ "path");
/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(path__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var copy_webpack_plugin__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! copy-webpack-plugin */ "copy-webpack-plugin");
/* harmony import */ var _lib_patterns__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./lib/patterns */ "./lib/patterns.ts");





class PyodidePlugin extends copy_webpack_plugin__WEBPACK_IMPORTED_MODULE_3__["default"] {
    globalLoadPyodide;
    constructor(options = {}) {
        let outDirectory = options.outDirectory || "pyodide";
        if (outDirectory.startsWith("/")) {
            outDirectory = outDirectory.slice(1);
        }
        const globalLoadPyodide = options.globalLoadPyodide || false;
        const pyodidePackagePath = tryGetPyodidePath(options.pyodideDependencyPath);
        const pkg = tryResolvePyodidePackage(pyodidePackagePath, options.version);
        options.patterns = _lib_patterns__WEBPACK_IMPORTED_MODULE_4__.chooseAndTransform(pkg.version, options.packageIndexUrl).map((pattern) => {
            return {
                from: path__WEBPACK_IMPORTED_MODULE_2___default().resolve(pyodidePackagePath, pattern.from),
                to: path__WEBPACK_IMPORTED_MODULE_2___default().join(outDirectory, pattern.to),
                transform: pattern.transform,
            };
        });
        assert__WEBPACK_IMPORTED_MODULE_1___default().ok(options.patterns.length > 0, `Unsupported version of pyodide. Must use >=${_lib_patterns__WEBPACK_IMPORTED_MODULE_4__.versions[0]}`);
        delete options.packageIndexUrl;
        delete options.globalLoadPyodide;
        super(options);
        this.globalLoadPyodide = globalLoadPyodide;
    }
    apply(compiler) {
        super.apply(compiler);
        if (this.globalLoadPyodide) {
            return;
        }
        // strip global loadPyodide
        compiler.hooks.make.tap(this.constructor.name, (compilation) => {
            compilation.hooks.succeedModule.tap({
                name: "pyodide-webpack-plugin",
                stage: compiler.webpack.Compilation.PROCESS_ASSETS_STAGE_OPTIMIZE_COMPATIBILITY,
            }, (module) => {
                const isPyodide = module.nameForCondition()?.match(/pyodide\/pyodide\.m?js$/);
                if (!isPyodide) {
                    return;
                }
                const source = module._source;
                source._value = source.source().replace("globalThis.loadPyodide=loadPyodide", "({})");
            });
        });
    }
}
/**
 * Try to find the pyodide path. Can't use require.resolve because it is not supported in
 * module builds. Nodes import.meta.resolve is experimental and still very new as of node 19.x
 * This method is works universally under the assumption of an install in node_modules/pyodide
 * @param pyodidePath
 * @returns
 */
function tryGetPyodidePath(pyodidePath) {
    if (pyodidePath) {
        return path__WEBPACK_IMPORTED_MODULE_2___default().resolve(pyodidePath);
    }
    const modulePath = path__WEBPACK_IMPORTED_MODULE_2___default().resolve("node_modules");
    for (const dependencyPath of fs__WEBPACK_IMPORTED_MODULE_0___default().readdirSync(path__WEBPACK_IMPORTED_MODULE_2___default().resolve("node_modules"), { withFileTypes: true })) {
        if (dependencyPath.name === "pyodide" && dependencyPath.isDirectory()) {
            return path__WEBPACK_IMPORTED_MODULE_2___default().join(modulePath, dependencyPath.name);
        }
    }
    throw new Error(`Unable to resolve pyodide package path in ${modulePath}`);
}
/**
 * Read the pyodide package dependency package.json to return necessary metadata
 * @param version
 * @returns
 */
function tryResolvePyodidePackage(pyodidePath, version) {
    if (version) {
        return { version };
    }
    const pkgPath = path__WEBPACK_IMPORTED_MODULE_2___default().resolve(pyodidePath, "package.json");
    try {
        const pkg = fs__WEBPACK_IMPORTED_MODULE_0___default().readFileSync(pkgPath, "utf-8");
        return JSON.parse(pkg);
    }
    catch (e) {
        throw new Error(`unable to read package.json from pyodide dependency in ${pkgPath}`);
    }
}
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PyodidePlugin);

})();

var __webpack_exports__PyodidePlugin = __webpack_exports__.PyodidePlugin;
var __webpack_exports__default = __webpack_exports__["default"];
export { __webpack_exports__PyodidePlugin as PyodidePlugin, __webpack_exports__default as default };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGx1Z2luLm1qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNPO0FBQ1A7QUFDQSxvQkFBb0IscUJBQXFCO0FBQ3pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ087QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlHQUF5RyxnQkFBZ0I7QUFDekgsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakIsS0FBSztBQUNMO0FBQ087QUFDUCw4RUFBOEUsUUFBUTtBQUN0RjtBQUNBOzs7Ozs7Ozs7OztBQzlEQSxlQUFlLFlBQVksNkJBQTZCO0FBQ3hEO0FBQ0E7Ozs7Ozs7Ozs7QUNGQTs7Ozs7Ozs7OztBQ0FBOzs7Ozs7Ozs7O0FDQUE7Ozs7OztTQ0FBO1NBQ0E7O1NBRUE7U0FDQTtTQUNBO1NBQ0E7U0FDQTtTQUNBO1NBQ0E7U0FDQTtTQUNBO1NBQ0E7U0FDQTtTQUNBO1NBQ0E7O1NBRUE7U0FDQTs7U0FFQTtTQUNBO1NBQ0E7Ozs7O1VDdEJBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQSxpQ0FBaUMsV0FBVztVQUM1QztVQUNBOzs7OztVQ1BBO1VBQ0E7VUFDQTtVQUNBO1VBQ0EseUNBQXlDLHdDQUF3QztVQUNqRjtVQUNBO1VBQ0E7Ozs7O1VDUEE7Ozs7O1VDQUE7VUFDQTtVQUNBO1VBQ0EsdURBQXVELGlCQUFpQjtVQUN4RTtVQUNBLGdEQUFnRCxhQUFhO1VBQzdEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ05vQjtBQUNRO0FBQ0o7QUFDcUI7QUFDRjtBQUNwQyw0QkFBNEIsMkRBQVU7QUFDN0M7QUFDQSw0QkFBNEI7QUFDNUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwyQkFBMkIsNkRBQTJCO0FBQ3REO0FBQ0Esc0JBQXNCLG1EQUFZO0FBQ2xDLG9CQUFvQixnREFBUztBQUM3QjtBQUNBO0FBQ0EsU0FBUztBQUNULFFBQVEsZ0RBQVMsNEVBQTRFLHNEQUFvQixDQUFDO0FBQ2xIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0dBQWtHO0FBQ2xHLGFBQWE7QUFDYixTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsbURBQVk7QUFDM0I7QUFDQSx1QkFBdUIsbURBQVk7QUFDbkMsaUNBQWlDLHFEQUFjLENBQUMsbURBQVksb0JBQW9CLHFCQUFxQjtBQUNyRztBQUNBLG1CQUFtQixnREFBUztBQUM1QjtBQUNBO0FBQ0EsaUVBQWlFLFdBQVc7QUFDNUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBLG9CQUFvQixtREFBWTtBQUNoQztBQUNBLG9CQUFvQixzREFBZTtBQUNuQztBQUNBO0FBQ0E7QUFDQSxrRkFBa0YsUUFBUTtBQUMxRjtBQUNBO0FBQ0EsaUVBQWUsYUFBYSxFQUFDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vQHB5b2RpZGUvd2VicGFjay1wbHVnaW4vLi9saWIvcGF0dGVybnMudHMiLCJ3ZWJwYWNrOi8vQHB5b2RpZGUvd2VicGFjay1wbHVnaW4vZXh0ZXJuYWwgbW9kdWxlIFwiY29weS13ZWJwYWNrLXBsdWdpblwiIiwid2VicGFjazovL0BweW9kaWRlL3dlYnBhY2stcGx1Z2luL2V4dGVybmFsIG5vZGUtY29tbW9uanMgXCJhc3NlcnRcIiIsIndlYnBhY2s6Ly9AcHlvZGlkZS93ZWJwYWNrLXBsdWdpbi9leHRlcm5hbCBub2RlLWNvbW1vbmpzIFwiZnNcIiIsIndlYnBhY2s6Ly9AcHlvZGlkZS93ZWJwYWNrLXBsdWdpbi9leHRlcm5hbCBub2RlLWNvbW1vbmpzIFwicGF0aFwiIiwid2VicGFjazovL0BweW9kaWRlL3dlYnBhY2stcGx1Z2luL3dlYnBhY2svYm9vdHN0cmFwIiwid2VicGFjazovL0BweW9kaWRlL3dlYnBhY2stcGx1Z2luL3dlYnBhY2svcnVudGltZS9jb21wYXQgZ2V0IGRlZmF1bHQgZXhwb3J0Iiwid2VicGFjazovL0BweW9kaWRlL3dlYnBhY2stcGx1Z2luL3dlYnBhY2svcnVudGltZS9kZWZpbmUgcHJvcGVydHkgZ2V0dGVycyIsIndlYnBhY2s6Ly9AcHlvZGlkZS93ZWJwYWNrLXBsdWdpbi93ZWJwYWNrL3J1bnRpbWUvaGFzT3duUHJvcGVydHkgc2hvcnRoYW5kIiwid2VicGFjazovL0BweW9kaWRlL3dlYnBhY2stcGx1Z2luL3dlYnBhY2svcnVudGltZS9tYWtlIG5hbWVzcGFjZSBvYmplY3QiLCJ3ZWJwYWNrOi8vQHB5b2RpZGUvd2VicGFjay1wbHVnaW4vLi9pbmRleC50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJjb25zdCBmaWxlcyA9IHtcbiAgICBcIjAuMjEuM1wiOiBbXG4gICAgICAgIFwiZGlzdHV0aWxzLnRhclwiLFxuICAgICAgICBcInBhY2thZ2UuanNvblwiLFxuICAgICAgICBcInB5b2RpZGVfcHkudGFyXCIsXG4gICAgICAgIFwicHlvZGlkZS5hc20uanNcIixcbiAgICAgICAgXCJweW9kaWRlLmFzbS5kYXRhXCIsXG4gICAgICAgIFwicHlvZGlkZS5hc20ud2FzbVwiLFxuICAgICAgICBcInJlcG9kYXRhLmpzb25cIixcbiAgICBdLFxuICAgIFwiMC4yMi4xXCI6IFtcbiAgICAgICAgXCJweW9kaWRlX3B5LnRhclwiLFxuICAgICAgICBcInB5b2RpZGUuYXNtLmpzXCIsXG4gICAgICAgIFwicHlvZGlkZS5hc20uZGF0YVwiLFxuICAgICAgICBcInB5b2RpZGUuYXNtLndhc21cIixcbiAgICAgICAgXCJyZXBvZGF0YS5qc29uXCIsXG4gICAgXSxcbn07XG5leHBvcnQgY29uc3QgdmVyc2lvbnMgPSBPYmplY3Qua2V5cyhmaWxlcyk7XG4vKipcbiAqIENob29zZSB0aGUgc2V0IG9mIGZpbGVzIHRvIG1hdGNoIGZvciBjb3B5aW5nIG91dCBvZiBweW9kaWRlLlxuICogQmFzZWQgb24gdGhlIHZlcnNpb24gcGFzc2VkLiBJZiBubyB2ZXJzaW9uIGlzIGF2YWlsYWJsZSBpbiBmaWxlcyB0byBtYXRjaFxuICogdGhhdCBpcyBncmVhdCBlbm91Z2ggYW4gZW1wdHkgYXJyYXkgaXMgcmV0dXJuZWQuXG4gKiBAcGFyYW0gdmVyc2lvblxuICogQHJldHVybnMge3N0cmluZ1tdfVxuICovXG5leHBvcnQgZnVuY3Rpb24gY2hvb3NlKHZlcnNpb24gPSBcIjAuMC4wXCIpIHtcbiAgICBsZXQgY2hvc2VuID0gW107XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCB2ZXJzaW9ucy5sZW5ndGg7IGkrKykge1xuICAgICAgICBpZiAodmVyc2lvbiA+PSB2ZXJzaW9uc1tpXSkge1xuICAgICAgICAgICAgY2hvc2VuID0gZmlsZXNbdmVyc2lvbnNbaV1dO1xuICAgICAgICB9XG4gICAgfVxuICAgIHJldHVybiBjaG9zZW47XG59XG4vKipcbiAqIENob29zZSB0aGUgc2V0IG9mIGZpbGVzIHRvIG1hdGNoIGZvciBjb3B5aW5nIG91dCBvZiBweW9kaWRlLlxuICogQmFzZWQgb24gdGhlIHZlcnNpb24gcGFzc2VkLiBJZiBubyB2ZXJzaW9uIGlzIGF2YWlsYWJsZSBpbiBmaWxlcyB0byBtYXRjaFxuICogdGhhdCBpcyBncmVhdCBlbm91Z2ggYW4gZW1wdHkgYXJyYXkgaXMgcmV0dXJuZWQuXG4gKiBAcGFyYW0gdmVyc2lvblxuICogQHBhcmFtIHBhdHRlcm5cbiAqIEBwYXJhbSBwYWNrYWdlSW5kZXhVcmxcbiAqIEByZXR1cm5zIHtQeW9kaWRlT2JqZWN0UGF0dGVybltdfVxuICovXG5leHBvcnQgZnVuY3Rpb24gdHJhbnNmb3JtKHZlcnNpb24sIHBhdHRlcm4sIHBhY2thZ2VJbmRleFVybCkge1xuICAgIHJldHVybiBwYXR0ZXJuLm1hcCgobmFtZSkgPT4ge1xuICAgICAgICBsZXQgdHJhbnNmb3JtO1xuICAgICAgICBpZiAocGFja2FnZUluZGV4VXJsICYmIG5hbWUgPT0gXCJweW9kaWRlLmFzbS5qc1wiKSB7XG4gICAgICAgICAgICB0cmFuc2Zvcm0gPSB7XG4gICAgICAgICAgICAgICAgdHJhbnNmb3JtZXI6IChpbnB1dCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gaW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgIC50b1N0cmluZygpXG4gICAgICAgICAgICAgICAgICAgICAgICAucmVwbGFjZShcInJlc29sdmVQYXRoKGZpbGVfbmFtZSxBUEkuY29uZmlnLmluZGV4VVJMKVwiLCBgcmVzb2x2ZVBhdGgoZmlsZV9uYW1lLFwiJHtwYWNrYWdlSW5kZXhVcmx9XCIpYCk7XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIH07XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHsgZnJvbTogbmFtZSwgdG86IG5hbWUsIHRyYW5zZm9ybSB9O1xuICAgIH0pO1xufVxuZXhwb3J0IGZ1bmN0aW9uIGNob29zZUFuZFRyYW5zZm9ybSh2ZXJzaW9uID0gXCIwLjAuMFwiLCBwYWNrYWdlSW5kZXhVcmwpIHtcbiAgICBwYWNrYWdlSW5kZXhVcmwgPSBwYWNrYWdlSW5kZXhVcmwgPz8gYGh0dHBzOi8vY2RuLmpzZGVsaXZyLm5ldC9weW9kaWRlL3Yke3ZlcnNpb259L2Z1bGwvYDtcbiAgICByZXR1cm4gdHJhbnNmb3JtKHZlcnNpb24sIGNob29zZSh2ZXJzaW9uKSwgcGFja2FnZUluZGV4VXJsKTtcbn1cbiIsInZhciB4ID0geSA9PiB7IHZhciB4ID0ge307IF9fd2VicGFja19yZXF1aXJlX18uZCh4LCB5KTsgcmV0dXJuIHg7IH1cbnZhciB5ID0geCA9PiAoKSA9PiB4XG5tb2R1bGUuZXhwb3J0cyA9IF9fV0VCUEFDS19FWFRFUk5BTF9NT0RVTEVfY29weV93ZWJwYWNrX3BsdWdpbl9jMWE5Zjg2ZF9fOyIsIm1vZHVsZS5leHBvcnRzID0gX19XRUJQQUNLX0VYVEVSTkFMX2NyZWF0ZVJlcXVpcmUoaW1wb3J0Lm1ldGEudXJsKShcImFzc2VydFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IF9fV0VCUEFDS19FWFRFUk5BTF9jcmVhdGVSZXF1aXJlKGltcG9ydC5tZXRhLnVybCkoXCJmc1wiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IF9fV0VCUEFDS19FWFRFUk5BTF9jcmVhdGVSZXF1aXJlKGltcG9ydC5tZXRhLnVybCkoXCJwYXRoXCIpOyIsIi8vIFRoZSBtb2R1bGUgY2FjaGVcbnZhciBfX3dlYnBhY2tfbW9kdWxlX2NhY2hlX18gPSB7fTtcblxuLy8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbmZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG5cdHZhciBjYWNoZWRNb2R1bGUgPSBfX3dlYnBhY2tfbW9kdWxlX2NhY2hlX19bbW9kdWxlSWRdO1xuXHRpZiAoY2FjaGVkTW9kdWxlICE9PSB1bmRlZmluZWQpIHtcblx0XHRyZXR1cm4gY2FjaGVkTW9kdWxlLmV4cG9ydHM7XG5cdH1cblx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcblx0dmFyIG1vZHVsZSA9IF9fd2VicGFja19tb2R1bGVfY2FjaGVfX1ttb2R1bGVJZF0gPSB7XG5cdFx0Ly8gbm8gbW9kdWxlLmlkIG5lZWRlZFxuXHRcdC8vIG5vIG1vZHVsZS5sb2FkZWQgbmVlZGVkXG5cdFx0ZXhwb3J0czoge31cblx0fTtcblxuXHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cblx0X193ZWJwYWNrX21vZHVsZXNfX1ttb2R1bGVJZF0obW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cblx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcblx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xufVxuXG4iLCIvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuX193ZWJwYWNrX3JlcXVpcmVfXy5uID0gKG1vZHVsZSkgPT4ge1xuXHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cblx0XHQoKSA9PiAobW9kdWxlWydkZWZhdWx0J10pIDpcblx0XHQoKSA9PiAobW9kdWxlKTtcblx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgeyBhOiBnZXR0ZXIgfSk7XG5cdHJldHVybiBnZXR0ZXI7XG59OyIsIi8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb25zIGZvciBoYXJtb255IGV4cG9ydHNcbl9fd2VicGFja19yZXF1aXJlX18uZCA9IChleHBvcnRzLCBkZWZpbml0aW9uKSA9PiB7XG5cdGZvcih2YXIga2V5IGluIGRlZmluaXRpb24pIHtcblx0XHRpZihfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZGVmaW5pdGlvbiwga2V5KSAmJiAhX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIGtleSkpIHtcblx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBrZXksIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBkZWZpbml0aW9uW2tleV0gfSk7XG5cdFx0fVxuXHR9XG59OyIsIl9fd2VicGFja19yZXF1aXJlX18ubyA9IChvYmosIHByb3ApID0+IChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqLCBwcm9wKSkiLCIvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG5fX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSAoZXhwb3J0cykgPT4ge1xuXHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcblx0fVxuXHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xufTsiLCJpbXBvcnQgZnMgZnJvbSBcImZzXCI7XG5pbXBvcnQgYXNzZXJ0IGZyb20gXCJhc3NlcnRcIjtcbmltcG9ydCBwYXRoIGZyb20gXCJwYXRoXCI7XG5pbXBvcnQgQ29weVBsdWdpbiBmcm9tIFwiY29weS13ZWJwYWNrLXBsdWdpblwiO1xuaW1wb3J0ICogYXMgcGF0dGVybnMgZnJvbSBcIi4vbGliL3BhdHRlcm5zXCI7XG5leHBvcnQgY2xhc3MgUHlvZGlkZVBsdWdpbiBleHRlbmRzIENvcHlQbHVnaW4ge1xuICAgIGdsb2JhbExvYWRQeW9kaWRlO1xuICAgIGNvbnN0cnVjdG9yKG9wdGlvbnMgPSB7fSkge1xuICAgICAgICBsZXQgb3V0RGlyZWN0b3J5ID0gb3B0aW9ucy5vdXREaXJlY3RvcnkgfHwgXCJweW9kaWRlXCI7XG4gICAgICAgIGlmIChvdXREaXJlY3Rvcnkuc3RhcnRzV2l0aChcIi9cIikpIHtcbiAgICAgICAgICAgIG91dERpcmVjdG9yeSA9IG91dERpcmVjdG9yeS5zbGljZSgxKTtcbiAgICAgICAgfVxuICAgICAgICBjb25zdCBnbG9iYWxMb2FkUHlvZGlkZSA9IG9wdGlvbnMuZ2xvYmFsTG9hZFB5b2RpZGUgfHwgZmFsc2U7XG4gICAgICAgIGNvbnN0IHB5b2RpZGVQYWNrYWdlUGF0aCA9IHRyeUdldFB5b2RpZGVQYXRoKG9wdGlvbnMucHlvZGlkZURlcGVuZGVuY3lQYXRoKTtcbiAgICAgICAgY29uc3QgcGtnID0gdHJ5UmVzb2x2ZVB5b2RpZGVQYWNrYWdlKHB5b2RpZGVQYWNrYWdlUGF0aCwgb3B0aW9ucy52ZXJzaW9uKTtcbiAgICAgICAgb3B0aW9ucy5wYXR0ZXJucyA9IHBhdHRlcm5zLmNob29zZUFuZFRyYW5zZm9ybShwa2cudmVyc2lvbiwgb3B0aW9ucy5wYWNrYWdlSW5kZXhVcmwpLm1hcCgocGF0dGVybikgPT4ge1xuICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICBmcm9tOiBwYXRoLnJlc29sdmUocHlvZGlkZVBhY2thZ2VQYXRoLCBwYXR0ZXJuLmZyb20pLFxuICAgICAgICAgICAgICAgIHRvOiBwYXRoLmpvaW4ob3V0RGlyZWN0b3J5LCBwYXR0ZXJuLnRvKSxcbiAgICAgICAgICAgICAgICB0cmFuc2Zvcm06IHBhdHRlcm4udHJhbnNmb3JtLFxuICAgICAgICAgICAgfTtcbiAgICAgICAgfSk7XG4gICAgICAgIGFzc2VydC5vayhvcHRpb25zLnBhdHRlcm5zLmxlbmd0aCA+IDAsIGBVbnN1cHBvcnRlZCB2ZXJzaW9uIG9mIHB5b2RpZGUuIE11c3QgdXNlID49JHtwYXR0ZXJucy52ZXJzaW9uc1swXX1gKTtcbiAgICAgICAgZGVsZXRlIG9wdGlvbnMucGFja2FnZUluZGV4VXJsO1xuICAgICAgICBkZWxldGUgb3B0aW9ucy5nbG9iYWxMb2FkUHlvZGlkZTtcbiAgICAgICAgc3VwZXIob3B0aW9ucyk7XG4gICAgICAgIHRoaXMuZ2xvYmFsTG9hZFB5b2RpZGUgPSBnbG9iYWxMb2FkUHlvZGlkZTtcbiAgICB9XG4gICAgYXBwbHkoY29tcGlsZXIpIHtcbiAgICAgICAgc3VwZXIuYXBwbHkoY29tcGlsZXIpO1xuICAgICAgICBpZiAodGhpcy5nbG9iYWxMb2FkUHlvZGlkZSkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIC8vIHN0cmlwIGdsb2JhbCBsb2FkUHlvZGlkZVxuICAgICAgICBjb21waWxlci5ob29rcy5tYWtlLnRhcCh0aGlzLmNvbnN0cnVjdG9yLm5hbWUsIChjb21waWxhdGlvbikgPT4ge1xuICAgICAgICAgICAgY29tcGlsYXRpb24uaG9va3Muc3VjY2VlZE1vZHVsZS50YXAoe1xuICAgICAgICAgICAgICAgIG5hbWU6IFwicHlvZGlkZS13ZWJwYWNrLXBsdWdpblwiLFxuICAgICAgICAgICAgICAgIHN0YWdlOiBjb21waWxlci53ZWJwYWNrLkNvbXBpbGF0aW9uLlBST0NFU1NfQVNTRVRTX1NUQUdFX09QVElNSVpFX0NPTVBBVElCSUxJVFksXG4gICAgICAgICAgICB9LCAobW9kdWxlKSA9PiB7XG4gICAgICAgICAgICAgICAgY29uc3QgaXNQeW9kaWRlID0gbW9kdWxlLm5hbWVGb3JDb25kaXRpb24oKT8ubWF0Y2goL3B5b2RpZGVcXC9weW9kaWRlXFwubT9qcyQvKTtcbiAgICAgICAgICAgICAgICBpZiAoIWlzUHlvZGlkZSkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGNvbnN0IHNvdXJjZSA9IG1vZHVsZS5fc291cmNlO1xuICAgICAgICAgICAgICAgIHNvdXJjZS5fdmFsdWUgPSBzb3VyY2Uuc291cmNlKCkucmVwbGFjZShcImdsb2JhbFRoaXMubG9hZFB5b2RpZGU9bG9hZFB5b2RpZGVcIiwgXCIoe30pXCIpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cbn1cbi8qKlxuICogVHJ5IHRvIGZpbmQgdGhlIHB5b2RpZGUgcGF0aC4gQ2FuJ3QgdXNlIHJlcXVpcmUucmVzb2x2ZSBiZWNhdXNlIGl0IGlzIG5vdCBzdXBwb3J0ZWQgaW5cbiAqIG1vZHVsZSBidWlsZHMuIE5vZGVzIGltcG9ydC5tZXRhLnJlc29sdmUgaXMgZXhwZXJpbWVudGFsIGFuZCBzdGlsbCB2ZXJ5IG5ldyBhcyBvZiBub2RlIDE5LnhcbiAqIFRoaXMgbWV0aG9kIGlzIHdvcmtzIHVuaXZlcnNhbGx5IHVuZGVyIHRoZSBhc3N1bXB0aW9uIG9mIGFuIGluc3RhbGwgaW4gbm9kZV9tb2R1bGVzL3B5b2RpZGVcbiAqIEBwYXJhbSBweW9kaWRlUGF0aFxuICogQHJldHVybnNcbiAqL1xuZnVuY3Rpb24gdHJ5R2V0UHlvZGlkZVBhdGgocHlvZGlkZVBhdGgpIHtcbiAgICBpZiAocHlvZGlkZVBhdGgpIHtcbiAgICAgICAgcmV0dXJuIHBhdGgucmVzb2x2ZShweW9kaWRlUGF0aCk7XG4gICAgfVxuICAgIGNvbnN0IG1vZHVsZVBhdGggPSBwYXRoLnJlc29sdmUoXCJub2RlX21vZHVsZXNcIik7XG4gICAgZm9yIChjb25zdCBkZXBlbmRlbmN5UGF0aCBvZiBmcy5yZWFkZGlyU3luYyhwYXRoLnJlc29sdmUoXCJub2RlX21vZHVsZXNcIiksIHsgd2l0aEZpbGVUeXBlczogdHJ1ZSB9KSkge1xuICAgICAgICBpZiAoZGVwZW5kZW5jeVBhdGgubmFtZSA9PT0gXCJweW9kaWRlXCIgJiYgZGVwZW5kZW5jeVBhdGguaXNEaXJlY3RvcnkoKSkge1xuICAgICAgICAgICAgcmV0dXJuIHBhdGguam9pbihtb2R1bGVQYXRoLCBkZXBlbmRlbmN5UGF0aC5uYW1lKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICB0aHJvdyBuZXcgRXJyb3IoYFVuYWJsZSB0byByZXNvbHZlIHB5b2RpZGUgcGFja2FnZSBwYXRoIGluICR7bW9kdWxlUGF0aH1gKTtcbn1cbi8qKlxuICogUmVhZCB0aGUgcHlvZGlkZSBwYWNrYWdlIGRlcGVuZGVuY3kgcGFja2FnZS5qc29uIHRvIHJldHVybiBuZWNlc3NhcnkgbWV0YWRhdGFcbiAqIEBwYXJhbSB2ZXJzaW9uXG4gKiBAcmV0dXJuc1xuICovXG5mdW5jdGlvbiB0cnlSZXNvbHZlUHlvZGlkZVBhY2thZ2UocHlvZGlkZVBhdGgsIHZlcnNpb24pIHtcbiAgICBpZiAodmVyc2lvbikge1xuICAgICAgICByZXR1cm4geyB2ZXJzaW9uIH07XG4gICAgfVxuICAgIGNvbnN0IHBrZ1BhdGggPSBwYXRoLnJlc29sdmUocHlvZGlkZVBhdGgsIFwicGFja2FnZS5qc29uXCIpO1xuICAgIHRyeSB7XG4gICAgICAgIGNvbnN0IHBrZyA9IGZzLnJlYWRGaWxlU3luYyhwa2dQYXRoLCBcInV0Zi04XCIpO1xuICAgICAgICByZXR1cm4gSlNPTi5wYXJzZShwa2cpO1xuICAgIH1cbiAgICBjYXRjaCAoZSkge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoYHVuYWJsZSB0byByZWFkIHBhY2thZ2UuanNvbiBmcm9tIHB5b2RpZGUgZGVwZW5kZW5jeSBpbiAke3BrZ1BhdGh9YCk7XG4gICAgfVxufVxuZXhwb3J0IGRlZmF1bHQgUHlvZGlkZVBsdWdpbjtcbiJdLCJuYW1lcyI6W10sInNvdXJjZVJvb3QiOiIifQ==