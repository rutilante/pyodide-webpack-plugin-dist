import CopyPlugin from "copy-webpack-plugin";
import { Compiler } from "webpack";
interface PyodideOptions extends Partial<CopyPlugin.PluginOptions> {
    /**
     * CDN endpoint for python packages
     * This option differs from
     * [loadPyodide indexUrl](https://pyodide.org/en/stable/usage/api/js-api.html)
     * in that it only impacts pip packages and _does not_ affect
     * the location the main pyodide runtime location. Set this value to "" if you want to keep
     * the pyodide default of accepting the indexUrl.
     *
     * @default https://cdn.jsdelivr.net/pyodide/v${installedPyodideVersion}/full/
     */
    packageIndexUrl?: string;
    /**
     * Whether or not to expose loadPyodide method globally. A globalThis.loadPyodide is useful when
     * using pyodide as a standalone script or in certain frameworks. With webpack we can scope the
     * pyodide package locally to prevent leaks (default).
     *
     * @default false
     */
    globalLoadPyodide?: boolean;
    /**
     * Relative path to webpack root where you want to output the pyodide files.
     * Defaults to pyodide
     */
    outDirectory?: string;
    /**
     * Pyodide package version to use when resolving the default pyodide package index url. Default
     * version is whatever version is installed in {pyodideDependencyPath}
     */
    version?: string;
    /**
     * Path on disk to the pyodide module. By default the plugin will attempt to look
     * in ./node_modules for pyodide.
     */
    pyodideDependencyPath?: string;
}
export declare class PyodidePlugin extends CopyPlugin {
    readonly globalLoadPyodide: boolean;
    constructor(options?: PyodideOptions);
    apply(compiler: Compiler): void;
}
export default PyodidePlugin;
//# sourceMappingURL=index.d.ts.map