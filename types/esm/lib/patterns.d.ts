import { ObjectPattern } from "copy-webpack-plugin";
interface PyodideObjectPattern extends ObjectPattern {
    to: string;
}
export declare const versions: string[];
/**
 * Choose the set of files to match for copying out of pyodide.
 * Based on the version passed. If no version is available in files to match
 * that is great enough an empty array is returned.
 * @param version
 * @returns {string[]}
 */
export declare function choose(version?: string): string[];
/**
 * Choose the set of files to match for copying out of pyodide.
 * Based on the version passed. If no version is available in files to match
 * that is great enough an empty array is returned.
 * @param version
 * @param pattern
 * @param packageIndexUrl
 * @returns {PyodideObjectPattern[]}
 */
export declare function transform(version: string, pattern: string[], packageIndexUrl: any): PyodideObjectPattern[];
export declare function chooseAndTransform(version?: string, packageIndexUrl?: string): PyodideObjectPattern[];
export {};
//# sourceMappingURL=patterns.d.ts.map